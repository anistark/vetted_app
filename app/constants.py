import os
_basedir = os.path.abspath(os.path.dirname(__file__))
import config

# General Responses to response codes
GENERAL_RESPONSE = {
    200 : 'Success',
    201 : 'Created',
    400 : 'Something went wrong! Please try again later!',
    401 : 'You are not Authorized.',
    403 : 'You are do not have access to access',
    404 : 'Data not found'
}


# Message response texts
MESSAGE_TEXT = {
    'SESSION_EXPIRED' : 'Your session seems to have expired. Kindly re-login to continue.'
}

