import sys
import traceback
from app import *
from app.models import *
import jwt, uuid, time, redis, bcrypt, re, elasticsearch, random, string, math, unicodedata
from datetime import datetime, timezone
from datetime import timedelta as timed
from sqlalchemy.orm import aliased
import itertools
from itertools import starmap
from sqlalchemy.exc import IntegrityError
from app import constants
import config
from random import choice
import calendar
import pytz
import boto3, botocore
from jinja2 import Environment, PackageLoader
import dateutil.parser
from dateutil.tz import *
import numpy as np
import timeit
import hashlib


char_set = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'

jinja2_env = Environment(
        loader=PackageLoader('app', 'templates')
)

# Insert in every exception end
# ex_type, ex, tb = sys.exc_info()
# print('traceback:', traceback.print_tb(tb))


def initialize_values():
    # response_code, response_data, err_msg, message
    return 200, '', '', ''


def handle_exception(e):
    if len(e.args) > 0:
        err_msg = e.args[0]
        if len(e.args) > 1:
            response_code = e.args[1]
        else:
            response_code = 400
    else:
        err_msg = 'Something wrong with server. Kindly Contact Admin'
        response_code = 400
    return response_code, err_msg


def send_response(response_code, response_data=None, err_msg=None, extra_response=False, message=None, structure=False):
    try:
        if response_code == 200 or response_code == 201 or response_code == 202 or response_code == 204:
            if extra_response:
                response = {'data': response_data, 'meta_data': extra_response}
            else:
                response = {'data': response_data}
            success = True
        else:
            if callable(err_msg):
                err_msg = constants.GENERAL_RESPONSE[response_code]
            response = {'error': err_msg}
            success = False
            db.session.rollback()
            db.session.close()
        if structure:
            if not response_data:
                response_data = None
            if not err_msg:
                err_msg = None
            structured_response = {
                'success': success,
                'status': response_code,
                'data': response_data,
                'errors': err_msg,
                'message': message
            }
            return make_response(jsonify(**structured_response), response_code)
        else:
            return make_response(jsonify(**response), response_code)
    except Exception as e:
        response_code = 400
        response = {
            'success': False,
            'status': response_code,
            'data': None,
            'errors': str(e),
            'message': 'Server Error. Kindly hold on.'
        }
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
        return make_response(jsonify(**response), response_code)


def special_to_dict(data):
    result_dict = {}
    for x in data.keys():
        result_dict[x] = getattr(data, x)
    return result_dict


def get_all_fields(structure=False):
    response_code, response_data, err_msg, message = initialize_values()
    try:
        response_data = list(map(lambda x: special_to_dict(x), FieldType.query.with_entities(FieldType.id, FieldType.name, FieldType.type).all()))
        if structure:
            structured_response = {}
            for response_item in response_data:
                structured_response[response_item['id']] = {
                    'name': response_item['name'],
                    'type': response_item['type']
                }
            response_data = structured_response
        response_code = 200
        message = 'All Fields!'
    except Exception as e:
        err_msg = str(e)
        message = 'Some Error occurred while fetching all fields'
        response_code = 400
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
    return response_code, response_data, err_msg, message


def get_question(question_id=None, all=False):
    response_code, response_data, err_msg, message = initialize_values()
    try:
        fields_response_code, fields_response_data, fields_err_msg, fields_message = get_all_fields(structure=True)
        if fields_response_code==200:
            fields_structured_response_data = fields_response_data
        if question_id:
            problem_data = Problem.query.get(question_id)
            if problem_data:
                response_data = problem_data.to_dict()
        else:
            if all:
                all_problem_data = list(map(lambda x: x.to_dict(), Problem.query.all()))
                if len(all_problem_data)>0:
                    response_data = all_problem_data
                if fields_response_code==200:
                    for response_item in response_data:
                        response_item['field_type_name'] = fields_structured_response_data[response_item['field_type_id']]
            else:
                print('get latest question')
        response_code = 200
        message = 'All Fields!'
    except Exception as e:
        err_msg = str(e)
        message = 'Some Error occurred while fetching all fields'
        response_code = 400
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
    return response_code, response_data, err_msg, message


def update_question(request_data, question_id=None):
    response_code, response_data, err_msg, message = initialize_values()
    try:
        if question_id:
            print('update question id:', question_id, ' | ', request_data)
        else:
            new_problem = Problem(title=request_data['title'], field_type_id=int(request_data['field_type_id']))
            db.session.add(new_problem)
            db.session.commit()
            response_data = {
                'success': True,
                'problem_id': new_problem.id
            }
        response_code = 200
        message = 'Question added!'
    except Exception as e:
        err_msg = str(e)
        message = 'Some Error occurred while fetching all fields'
        response_code = 400
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
    db.session.close()
    return response_code, response_data, err_msg, message


def add_submission(request_data):
    response_code, response_data, err_msg, message = initialize_values()
    try:
        new_submission = Submission(problem_id=request_data['problem_id'], value=request_data['value'])
        db.session.add(new_submission)
        db.session.commit()
        response_data = {
            'success': True,
            'submission_id': new_submission.id
        }
        response_code = 200
        message = 'Submission added!'
    except Exception as e:
        err_msg = str(e)
        message = 'Some Error occurred while fetching all fields'
        response_code = 400
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
    db.session.close()
    return response_code, response_data, err_msg, message


def get_submission(problem_id=None, submission_id=None):
    response_code, response_data, err_msg, message = initialize_values()
    try:
        if problem_id:
            all_submissions = list(map(lambda x: x.to_dict(), Submission.query.filter_by(problem_id=problem_id).all()))
            if len(all_submissions)>0:
                response_data = all_submissions
        elif submission_id:
            submission_data = Submission.query.get(submission_id)
            if submission_data:
                response_data = submission_data.to_dict()
        else:
            print('all')
        response_code = 200
        message = 'All Submissions!'
    except Exception as e:
        err_msg = str(e)
        message = 'Some Error occurred while fetching all fields'
        response_code = 400
        ex_type, ex, tb = sys.exc_info()
        print('traceback:', traceback.print_tb(tb))
    return response_code, response_data, err_msg, message
