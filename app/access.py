import os
import config
from app.helpers import *
from app.models import *
from sqlalchemy.orm import aliased


_basedir = os.path.abspath(os.path.dirname(__file__))


def is_admin(member_id):
    admin_state = False
    member_info = Member.query.get(member_id)
    if member_info:
        member_info = member_info.to_dict()
        if member_info['type'] == 0:
            admin_state = True
        else:
            admin_state = False
    return admin_state


def has_access(member_id, gang_id=False, activity_id=False, checkpoint_id=False, module_id=False):
    response_state = True
    if gang_id:
        gang = Gang.query.get(gang_id)
        if int(gang.member_id) == int(member_id):
            response_state = True
        else:
            response_state = False
    if activity_id:
        AVT = aliased(Activity)
        GNG = aliased(Gang)
        activities_data = (db.session.query(AVT, GNG)
                           .join(GNG, AVT.gang_id == GNG.id)
                           .with_entities(AVT.id.label("activity_id"), AVT.gang_id.label("activity_gang_id"), GNG.id.label("gang_id"), GNG.member_id.label("member_id"))
                           .filter(AVT.id == activity_id)
                           .all()
                           )
        if len(activities_data) > 0:
            response_state = True
        else:
            response_state = False
    if checkpoint_id:
        AVT = aliased(Activity)
        CPN = aliased(Checkpoint)
        GNG = aliased(Gang)
        checkpoint_data = (db.session.query(CPN, AVT, GNG)
                           .join(AVT, CPN.activity_id == AVT.id)
                           .join(GNG, AVT.gang_id == GNG.id)
                           .with_entities(AVT.id.label('activity_id'),GNG.member_id.label("member_id"))
                           .filter(CPN.id == checkpoint_id)
                           .all()
                           )
        if len(checkpoint_data) > 0:
            response_state = True
        else:
            response_state = False
    return response_state


def is_host(member_id, gang_id=False, activity_id=False, checkpoint_id=False):
    manager_state = False
    if gang_id:
        pass
    elif activity_id:
        activity_data = Activity.query.get(activity_id)
        if activity_data:
            gang_id = activity_data.gang_id
    elif checkpoint_id:
        checkpoint_data = Checkpoint.query.get(checkpoint_id)
        if checkpoint_data:
            activity_data = Activity.query.get(checkpoint_data.activity_id)
            if activity_data:
                gang_id = activity_data.gang_id
                if len(activity_data.checkpoint_ids)>0:
                    if int(checkpoint_id) in (list(map(lambda x: int(x), activity_data.checkpoint_ids.values()))):
                        activity_id = activity_data.id
    if activity_id:
        access_gang_ids = []
        if member_id:
            member_gang_check = list(map(lambda x: x.to_dict(), MemberGangAccess.query.filter_by(member_id=member_id).all()))
            if len(member_gang_check)>0:
                for member_gang_access in member_gang_check:
                    access_gang_ids.append(member_gang_access)
            # Check for co-hosts activity_data.id
            co_host_gangs_check = list(map(lambda x: x.to_dict(), ActivityGangAccess.query.filter_by(activity_id=activity_id).all()))
            if co_host_gangs_check:
                for co_host_gang in co_host_gangs_check:
                    manager_state, access_type = is_manager(member_id, co_host_gang.gang_id)
                    if manager_state:
                        return manager_state
            else:
                manager_state, access_type = is_manager(member_id, gang_id)
                if manager_state:
                    return manager_state
    return manager_state


def is_judge(member_id, problem_id=False):
    judge_state = False
    if problem_id:
        problem_data = Problem.query.get(problem_id)
        if problem_data:
            problem_setup_value_check = ProblemSetupValue.query.filter_by(problem_id=problem_id).filter_by(problem_setup_field_id=42).first()
            if problem_setup_value_check:
                problem_setup_value_check = problem_setup_value_check.to_dict()
                if member_id in problem_setup_value_check['values']:
                    judge_state = True
    return judge_state


def is_manager(member_id, gang_id, access_type=False):
    manager_state = False
    try:
        if gang_id:
            gang_data = Gang.query.get(gang_id)
            if gang_data:
                if int(member_id) == int(gang_data.member_id):
                    manager_state = True
                    access_type = -1
                else:
                    all_member_gang_access_data = list(map(lambda x: x.to_dict(), MemberGangAccess.query.filter_by(member_id=member_id).filter_by(gang_id=gang_id).all()))
                    if len(all_member_gang_access_data)>0:
                        for member_gang_access_data in all_member_gang_access_data:
                            if int(member_id)==int(member_gang_access_data['member_id']):
                                access_type = member_gang_access_data['type']
                                if int(member_gang_access_data['type'])==1:
                                    manager_state = True
    except Exception as e:
        print('In is_manager Exception'+ str(e))
    return manager_state, access_type
