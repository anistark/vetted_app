import app
from flask import Blueprint, jsonify, request, make_response, render_template, send_from_directory, redirect, url_for
import json
from app.helpers import *
from cerberus import Validator

validator = Validator()

main = Blueprint('main', __name__)

# query_params = request.args.to_dict()
# request_data = json.loads(request.data)


@main.route('/')
def index():
    return 'Welcome to '+config.PLATFORM_NAME+' Platform'
