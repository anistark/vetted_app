from flask import Flask, make_response, jsonify, json, request
from app.main.controllers import main
from app.api.controllers import api
from flask_sqlalchemy import SQLAlchemy, get_state
from flask_cors import CORS
import config, logging, time, traceback, uuid, pytz
from logging.handlers import RotatingFileHandler
from time import strftime
from datetime import datetime, timezone
import redis
from flask_compress import Compress

app = Flask(__name__)
Compress(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.SQLALCHEMY_TRACK_MODIFICATIONS
# print('SQLALCHEMY_DATABASE_URI', config.SQLALCHEMY_DATABASE_URI)

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

db = SQLAlchemy(app)

app.register_blueprint(api, url_prefix='/api/v1')
app.register_blueprint(main, url_prefix='/')


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': str(error)}), 404)


@app.errorhandler(500)
def internal_error(exception):
    # print('Error handler => '+str(exception))
    return make_response(jsonify({'error': 'Server Fatal Error! Kindly Hang on!'}), 404)


@app.errorhandler(Exception)
def unhandled_exception(exception):
    # print('Unhandled Error handler => ' + str(exception))
    return make_response(jsonify({'error': 'Server Fatal Error! Kindly Hang on!!'}), 404)


@app.after_request
def after_request(response):
    timestamp = strftime('%Y-%b-%d %H:%M')
    try:
        request_data = None
        if request.data:
            request_data = json.loads(request.data)
        else:
            request_data = None
        log_data = {
            'request_method': request.method,
            'request_full_path': request.full_path,
            'request_data_body': request_data,
            'request_data_args': request.args.to_dict(),
            'response_status': response.status,
            'response_data': response.data.decode('utf-8'),
            'request_headers': dict(request.headers),
            'timestamp': time.time(),
            'log_time_read': datetime.fromtimestamp(int(time.time()), pytz.timezone(
                    'Asia/Kolkata')).isoformat(),
            'request_remote_addr': request.remote_addr
        }
        # print('ping after => ', time.time())
    except Exception as e:
        print('Attention Required!! Some Error in Logging')
        print('Exception in after request => '+str(e))
    return response


@app.errorhandler(Exception)
def exceptions(e):
    tb = traceback.format_exc()
    timestamp = strftime('[%Y-%b-%d %H:%M]')
    # print('%s %s %s %s %s 5xx INTERNAL SERVER ERROR\n%s', timestamp, request.remote_addr, request.method, request.scheme, request.full_path, tb)
    return e


@app.before_request
def before_request():
    try:
        log_data = {
            'request_headers': dict(request.headers),
            'request_full_path': request.full_path,
            'request_data': request.data.decode('utf-8'),
            'request_data_args': request.args.to_dict(),
            'request_method': request.method,
            'timestamp': time.time(),
            'log_time_read': datetime.fromtimestamp(int(time.time()), pytz.timezone('Asia/Kolkata')).isoformat(),
            'request_remote_addr': request.remote_addr
        }
        # print('ping before => ', time.time())
    except Exception as e:
        print('Attention Required!! Some Error in Logging')
        print('Exception in before request => '+str(e))
