from flask import Flask
import config
import os
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
# SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:password@localhost/test'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = config.SQLALCHEMY_TRACK_MODIFICATIONS
app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI

db = SQLAlchemy(app)
