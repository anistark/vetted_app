import os

from datetime import datetime
from pytz import timezone


IST = timezone('Asia/Kolkata')
UTC = timezone('UTC')


def to_tz(t, tz=UTC):
    try:
        if t:
            tmz = t.astimezone(tz).isoformat()
        else:
            tmz = t
    except Exception as e:
        # print(e)
        tmz = t
    return tmz


from app.database import *


class FieldType(db.Model):
    __tablename__ = 'field_type'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), index=True)
    type = db.Column(db.String(), index=True)
    created_at = db.Column(db.DateTime(timezone=True), default=datetime.now)
    updated_at = db.Column(db.DateTime(timezone=True), default=datetime.now, onupdate=datetime.now)

    def __init__(self, name, type):
        self.name = name
        self.type = type

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'type': self.type,
            'created_at': to_tz(self.created_at, UTC),
            'updated_at': to_tz(self.updated_at, UTC)
        }


class Problem(db.Model):
    __tablename__ = 'problem'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String())
    field_type_id = db.Column(db.Integer, db.ForeignKey('field_type.id', onupdate='CASCADE', ondelete='SET NULL'), nullable=True, index=True)
    created_at = db.Column(db.DateTime(timezone=True), default=datetime.now)
    updated_at = db.Column(db.DateTime(timezone=True), default=datetime.now, onupdate=datetime.now)

    def __init__(self, title, field_type_id):
        self.title = title
        self.field_type_id = field_type_id

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def to_dict(self):
        return{
            'id': self.id,
            'title': self.title,
            'field_type_id': self.field_type_id,
            'created_at': to_tz(self.created_at, UTC),
            'updated_at': to_tz(self.updated_at, UTC)
        }


class Submission(db.Model):
    __tablename__ = 'submission'

    id = db.Column(db.Integer, primary_key=True)
    problem_id = db.Column(db.Integer, db.ForeignKey('problem.id', onupdate='CASCADE', ondelete='SET NULL'), nullable=True, index=True)
    value = db.Column(db.String(), index=True)
    created_at = db.Column(db.DateTime(timezone=True), default=datetime.now)
    updated_at = db.Column(db.DateTime(timezone=True), default=datetime.now, onupdate=datetime.now)

    def __init__(self, problem_id, value):
        self.problem_id = problem_id
        self.value = value

    def __repr__(self):
        return '<id {}>'.format(self.id)

    def to_dict(self):
        return {
            'id': self.id,
            'problem_id': self.problem_id,
            'value': self.value,
            'created_at': to_tz(self.created_at, UTC),
            'updated_at': to_tz(self.updated_at, UTC)
        }
