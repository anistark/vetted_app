import app
from flask import Blueprint, jsonify, request, make_response, render_template, send_from_directory, redirect, url_for
import json
from app.helpers import *
from cerberus import Validator

validator = Validator()

api = Blueprint('api', __name__)

# query_params = request.args.to_dict()
# request_data = json.loads(request.data)


@api.route('/')
def index():
    return 'Welcome to '+config.PLATFORM_NAME+' Platform'


@api.route('/fields', methods=['GET'])
def fields():
    response_code, response_data, err_msg, message = initialize_values()
    try:
        response_code, response_data, err_msg, message = get_all_fields()
    except Exception as e:
        response_code, err_msg = handle_exception(e)
    if isinstance(response_data,int):
        response_data = constants.GENERAL_RESPONSE[response_code]
        err_msg = constants.GENERAL_RESPONSE[response_code]
    return send_response(response_code, response_data, err_msg)


@api.route('/question', methods=['GET', 'POST'])
def question():
    response_code, response_data, err_msg, message = initialize_values()
    try:
        if request.method == 'GET':
            query_params = request.args.to_dict()
            if 'question_id' in query_params:
                response_code, response_data, err_msg, message = get_question(question_id=query_params['question_id'])
            else:
                response_code, response_data, err_msg, message = get_question()
        elif request.method == 'POST':
            request_data = json.loads(request.data)
            response_code, response_data, err_msg, message = update_question(request_data)
    except Exception as e:
        response_code, err_msg = handle_exception(e)
    if isinstance(response_data,int):
        response_data = constants.GENERAL_RESPONSE[response_code]
        err_msg = constants.GENERAL_RESPONSE[response_code]
    return send_response(response_code, response_data, err_msg)


@api.route('/question/all', methods=['GET'])
def all_questions():
    response_code, response_data, err_msg, message = initialize_values()
    try:
        if request.method == 'GET':
            query_params = request.args.to_dict()
            response_code, response_data, err_msg, message = get_question(all=True)
    except Exception as e:
        response_code, err_msg = handle_exception(e)
    if isinstance(response_data,int):
        response_data = constants.GENERAL_RESPONSE[response_code]
        err_msg = constants.GENERAL_RESPONSE[response_code]
    return send_response(response_code, response_data, err_msg)


@api.route('/submission', methods=['GET', 'POST'])
def submission():
    response_code, response_data, err_msg, message = initialize_values()
    try:
        if request.method == 'GET':
            query_params = request.args.to_dict()
            print('query_params:', query_params)
            if 'question_id' in query_params:
                response_code, response_data, err_msg, message = get_submission(problem_id=query_params['question_id'])
            elif 'submission_id' in query_params:
                response_code, response_data, err_msg, message = get_submission(submission_id=query_params['submission_id'])
            else:
                print('ss')
        elif request.method == 'POST':
            request_data = json.loads(request.data)
            response_code, response_data, err_msg, message = add_submission(request_data)
    except Exception as e:
        response_code, err_msg = handle_exception(e)
    if isinstance(response_data,int):
        response_data = constants.GENERAL_RESPONSE[response_code]
        err_msg = constants.GENERAL_RESPONSE[response_code]
    return send_response(response_code, response_data, err_msg)
