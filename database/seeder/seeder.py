import os
# goal_dir = os.path.join(os.getcwd(), "../../")
os.sys.path.insert(0, os.path.abspath(os.getcwd()))

from app.database import db

from app.models import *

tables = [
    {
        "model": FieldType,
        "columns": ["name", "type"],
        "rows": [["input", "text"],
                 ["number", "number"],
                 ["date", "date"],
                 ["boolean", "bool"]]
    }
]


def reflect(*args, **kwargs):
    print(args)
    print(kwargs)


def main():
    # start adding
    for table in tables:
        table_columns = table['columns']
        table_model = table['model']
        table_rows = table['rows']
        table_rows_count = len(table_model.query.all())
        c = 0
        for row in table_rows:
            row_obj = dict()
            for index, column in enumerate(table_columns):
                row_obj[column] = row[index]
            new_row = table_model(**row_obj)
            # insert new_row
            if table_rows_count <= c:
                db.session.add(new_row)
                db.session.commit()
            c = c+1
if __name__ == '__main__':
    main()
