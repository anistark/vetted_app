import os
_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True

PRODUCTION = False

SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8000

BASE_CLIENT_URL = 'http://localhost:9000/'

SECRET_KEY = '845D32B31BAF419A9F'
ACCESS_KEY = '96BBCD92E4441'

ENCODING_ALGORITHM = 'HS256'

# Platform General Info
PLATFORM_NAME = 'Vetted'

DB_CONFIG = {
    'local': {
        'driver': 'postgresql',
        'username': 'postgres',
        'password': 'password',
        'host': 'localhost',
        'port': '5432',
        'db_name': 'vetted'
    }
}

# SQLALCHEMY Config
SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_DATABASE_URI = DB_CONFIG['local']['driver']+'://'+DB_CONFIG['local']['username']+':'+DB_CONFIG['local']['password']+'@'+DB_CONFIG['local']['host']+':'+DB_CONFIG['local']['port']+'/'+DB_CONFIG['local']['db_name']

THREADS_PER_PAGE = 8

WTF_CSRF_ENABLED = True
WTF_CSRF_SECRET_KEY = "somethingimpossibletoguess"

RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'
RECAPTCHA_OPTIONS = {'theme': 'white'}

SESSION_TIME = 31536000  # 1 year = 24*60*60*365

RESET_EXPIRY_TIME = 86400  # 1 day = 24*60*60
