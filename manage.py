import os
goal_dir = os.path.join(os.getcwd(), "../../")
os.sys.path.insert(0, os.path.abspath(goal_dir))

from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app.database import *
from app.models import *

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()

