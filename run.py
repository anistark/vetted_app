# Run server
from app import app
import config
# from pyfiglet import Figlet

if __name__ == "__main__":
    # f = Figlet(font='lean')
    # print(f.renderText('Skillenza'))
    app.run(host=config.SERVER_HOST, port=config.SERVER_PORT, debug=config.DEBUG)
