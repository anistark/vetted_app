# Platform - Backend


## Project Structure
    
    ├───run.py
    ├───config.py
    ├───bin
    ├───app
    │   ├─ __init__.py
    │   ├───main
    │   │        __init__.py
    │   │        controllers.py
    │   ├─ __init__.py
    │   ├─ constants.py
    │   ├─ database.py
    │   ├─ helpers.py
    │   └─ models.py
    │
    ├───database
    │   ├───migrations
    │   │       __init__.py
    │   │       migrate.py
    │   └───seeder
    │           __init__.py
    │           seeder.py
    │───tests
    │      __init__.py
    │      __db_test__.py
    __init__.py
    
    Brief Description of the Application Structure
    
    bin – in this folder all the scripts, that will be executed on the command line will be placed
    
    tests – this folder contains the unit tests
    app – contains the application itself
    database - in this folder all the migration and seeding scripts,
    run.py – this file is used to run the Flask application, where the contents of the file is


### To run :

```
python run.py
```

### Using Gunicorn:

Kill a running instance :

```
pkill -9 gunicorn
```

Start a new instance: 

```
gunicorn app:app --workers=4 --name platform --log-file logs/gunicorn.log --reload &
```


### [Api Collection][1]


### Database Initialize ( Just the first time )

```
python manage.py db init
```

#### Database Migrations

```
python manage.py db migrate && python manage.py db upgrade
```

#### Database Seeding

```
python database/seeder/seeder.py
```


[1]: https://www.getpostman.com/collections/fa11749ed1c60caaf2f9
